class Movie {
  final double popularity;
  final int id;
  final bool video;
  final int vote_count;
  final double vote_average;
  final String title;
  final String release_date;
  final String original_language;
  final String original_title;
  final List<dynamic> genre_ids3;
  final String backdrop_path;
  final bool adult;
  final String overviewe;
  final String poster_path;
  final String baseImageUrl = 'https://image.tmdb.org/t/p/w500';

  Movie({
    this.popularity,
    this.id,
    this.video,
    this.vote_count,
    this.vote_average,
    this.title,
    this.release_date,
    this.original_language,
    this.original_title,
    this.genre_ids3,
    this.backdrop_path,
    this.adult,
    this.overviewe,
    this.poster_path,
});
}