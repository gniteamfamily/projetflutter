import 'package:flutter/material.dart';
import 'package:monApp/api.dart';
import 'package:monApp/detail.dart';
import 'package:monApp/login.dart';
import 'package:monApp/movie_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.grey,

        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LoginPage(),
    );
  }

}


class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  Future<List<Movie>> monService = HttpService().getPosts();

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final HttpService http = HttpService();
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      backgroundColor: Color.fromRGBO(44, 44, 44, 0.4),

      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),

      ),

      body: FutureBuilder(
        future: http.getPosts(),
        builder: (BuildContext context, AsyncSnapshot<List<Movie>> snapshot) {
          if (snapshot.hasData) {
            List<Movie> listeFilm = snapshot.data;

            return SingleChildScrollView(

              child: Container(
                height: 350,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: listeFilm.length,
                  itemBuilder: (x,y) =>
                  Column(
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.all(5.0),
                        child: Container(
                          height: 320,
                          width: 130,

                          child: GestureDetector(

                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute<void>(
                                      builder:(BuildContext context) {
                                        return Detail(numeroDuFilm: listeFilm[y].id);
                                      }));
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(listeFilm[y].baseImageUrl+listeFilm[y].poster_path)
                                )
                              ),
                            )
                          ),
                        ),
                      ),
                      Text(
                        listeFilm[y].title,
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),

                      )

                    ],


                    )
                  ),

                ),
              );
          }
          return Center(child: new CircularProgressIndicator());
        }
  ));
}
}