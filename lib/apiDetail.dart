import 'dart:convert';

import 'package:http/http.dart';
import 'package:monApp/detail_model.dart';

class HttpService {


  Future<DetailModel>getPostsDetail(String monFilm) async {
    String postUrl = 'https://api.themoviedb.org/3/movie/'+monFilm+'?api_key=62feaff3d2cf094a340f530fbf25bde9&language=fr-FR';
    Response res = await get(postUrl);
    print(res);
    if(res.statusCode == 200){
      Map<String, dynamic> body = jsonDecode(res.body);
      //dynamic result = body['results'];
      DetailModel detail = DetailModel(
        adult : body['adult'],
        genres : body['genres'],
        original_language : body['original_language'],
        original_title : body['original_title'],
        overview : body['overview'],
        popularity : body['popularity'].toDouble(),
        realease_date : body['release_date'],
        title : body['title'],
        vote_average : body['vote_average'].toDouble(),
        vote_count : body['vote_count'],
        backdrop_patch: body['backdrop_path'],
      );

      return detail;
    }
    else{
      throw 'MESSAGE DERREUR';
    }

  }
}