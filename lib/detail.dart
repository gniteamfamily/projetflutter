import 'package:flutter/material.dart';
import 'package:monApp/apiDetail.dart';
import 'package:monApp/detail_model.dart';
import 'package:monApp/main.dart';


class Detail extends StatelessWidget {
  final int numeroDuFilm;
  Detail({@required this.numeroDuFilm});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: DetailPage(title: 'Detail',numFilm : numeroDuFilm),
    );
  }

}


class DetailPage extends StatefulWidget {
  DetailPage({Key key, this.title, this.numFilm}) : super(key: key);

  final String title;
  final int numFilm;

  @override
  _DetailPage createState() => _DetailPage();
}

class _DetailPage extends State<DetailPage> {
  final HttpService http = HttpService();
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Color.fromRGBO(44, 44, 44, 0.4),
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () =>
                Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage(title: 'Liste Films'))),
          ),
        ],
      ),
      body: FutureBuilder(
        future: http.getPostsDetail(widget.numFilm.toString()),
        builder: (BuildContext context, AsyncSnapshot<DetailModel> snapshot) {
          if(snapshot.hasData) {
            DetailModel detail = snapshot.data;
            return Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.max,
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height / 2,
                        width: 1050,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            image: DecorationImage(
                              alignment: Alignment.topCenter,
                              fit: BoxFit.cover,
                              image :  NetworkImage(detail.baseImageUrl+detail.backdrop_patch),
                            )
                        ),
                      ),
                      Container(
                        child: Text(
                          detail.title,
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 40.0,
                            color: Colors.white,
                          ),
                        ),

                      ),
                      Container(
                        child: Text(
                          '15 + '+detail.realease_date+' - '+detail.vote_average.toString(),
                          style: new TextStyle(
                            fontSize: 15.0,
                            color: Colors.white,
                          ),
                        ),

                      ),
                      Row(
                        children: <Widget>[
                          new Expanded(
                              child: new Container(
                                width: 150,
                                height: 30,
                                alignment: Alignment.center,
                                child: new Text('Crime',style: new TextStyle(color: Colors.black,fontFamily: 'Arvo',fontSize: 15.0),),
                                decoration: new BoxDecoration(
                                    borderRadius: new BorderRadius.circular(10.0),
                                    color: const Color(0xFFFFFFFF)
                                ),
                              )
                          ),
                          new Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: new Container(
                              width: 150.0,
                              height: 30.0,
                              alignment: Alignment.center,
                              child: new Text('Drama',style: new TextStyle(color: Colors.black,fontFamily: 'Arvo',fontSize: 15.0),),
                              decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(10.0),
                                  color: const Color(0xFFFFFFFF)
                              ),
                            ),
                          ),
                          new Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: new Container(
                              width: 150.0,
                              height: 30.0,
                              alignment: Alignment.center,
                              child: new Text('Mystery',style: new TextStyle(color: Colors.black,fontFamily: 'Arvo',fontSize: 15.0),),
                              decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(10.0),
                                  color: const Color(0xFFFFFFFF)
                              ),
                            ),
                          )
                        ],
                      ),
                      Container(
                        height: 40,
                        child: Text(
                          'cast : Millie Bobby Brown Henry Cavill Sam Clafin Helena Bonham Carter',
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Container(
                        height: 25,
                        child: Text(
                          'Summary',
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15.0,
                            color: Colors.white,
                          ),
                        ),

                      ),
                      Container(
                        child: Text(
                        detail.overview,
                          style: new TextStyle(
                            fontSize: 12.0,
                            color: Colors.white,
                          ),
                        ),

                      ),
                    ]
                ),
            );
          }
          return Center(child: new CircularProgressIndicator());
        }
      )



    );
  }


}
