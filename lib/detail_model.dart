class DetailModel {
  final bool adult;
  final List<dynamic> genres;
  final String original_language;
  final String original_title;
  final String overview;
  final double popularity;
  final String realease_date;
  final String title;
  final double vote_average;
  final int vote_count;
  final String backdrop_patch;
  final String baseImageUrl = 'https://image.tmdb.org/t/p/w500';

  DetailModel({
    this.adult,
    this.genres,
    this.original_language,
    this.original_title,
    this.overview,
    this.popularity,
    this.realease_date,
    this.title,
    this.vote_average,
    this.vote_count,
    this.backdrop_patch
  });
}