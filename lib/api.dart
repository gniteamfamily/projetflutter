import 'dart:convert';

import 'package:http/http.dart';
import 'package:monApp/movie_model.dart';

class HttpService {
  final String postUrl = 'https://api.themoviedb.org/3/movie/popular?api_key=62feaff3d2cf094a340f530fbf25bde9&language=fr-FR&page=1';

  Future<List<Movie>>getPosts() async {
    Response res = await get(postUrl);
  print(res);
    if(res.statusCode == 200){
      Map<String, dynamic> body = jsonDecode(res.body);
      List<dynamic> result = body['results'];
      List<Movie> movies = List<Movie>();
      result.forEach((element) {
        movies.add(Movie(
          adult: element['adult'],
          backdrop_path: element['backdrop_path'],
          genre_ids3: element['genre_ids3'],
          id: element['id'],
          original_language: element['original_language'],
          original_title: element['original_title'],
          overviewe: element['overviewe'],
          popularity: element['popularity'].toDouble(),
          poster_path: element['poster_path'],
          release_date: element['release_date'],
          title: element['title'],
          video: element['video'],
          vote_average: element['vote_average'].toDouble(),
          vote_count: element['vote_count'],
        ));
      });
      return movies;
    }
    else{
      throw 'MESSAGE DERREUR';
    }

  }
}